using System;
using UnityEngine.Events;

[Serializable]
public class CustomEvent
{
	public UnityEvent unityEvent;

	public virtual void Do ()
	{
		unityEvent.Invoke();
	}
}