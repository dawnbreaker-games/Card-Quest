using System;
using Extensions;
using UnityEngine;

namespace CardQuest
{
	[CreateAssetMenu]
	public class IslandsLevelsData : ScriptableObject
	{
		public IslandsLevelZone[] levelZones = new IslandsLevelZone[0];
		public IslandsLevelEntry[] islandsLevelEntries = new IslandsLevelEntry[0];

		[Serializable]
		public class IslandsLevelEntry
		{
			public string name;
			public Vector2Int dimensions;
			public int cardCount = 6;
			public int cardTypeCount = 1;
			public int islandCount = 2;
			public int moveCount = 1;
			public CardModifier[] cardModifiers = new CardModifier[0];
			public Sprite backgroundSprite;
			public Rect[] islandRects = new Rect[0];
			public Vector2 cameraOffset;
			public float timePerMove;
			public AudioClip[] musics = new AudioClip[0];

			public IslandsLevelEntry (IslandsLevelEntry islandsLevelEntry)
			{
				name = islandsLevelEntry.name;
				dimensions = islandsLevelEntry.dimensions;
				cardCount = islandsLevelEntry.cardCount;
				cardTypeCount = islandsLevelEntry.cardTypeCount;
				moveCount = islandsLevelEntry.moveCount;
				cardModifiers = islandsLevelEntry.cardModifiers;
				backgroundSprite = islandsLevelEntry.backgroundSprite;
				islandRects = islandsLevelEntry.islandRects;
				cameraOffset = islandsLevelEntry.cameraOffset;
				timePerMove = islandsLevelEntry.timePerMove;
				musics = islandsLevelEntry.musics;
			}

			public IslandsLevelEntry (string name, Vector2Int dimensions, int cardCount, int cardTypeCount, int islandCount, int moveCount, CardModifier[] cardModifiers, Sprite backgroundSprite, Rect[] islandRects, Vector2 cameraOffset, float timePerMove, AudioClip[] musics)
			{
				this.name = name;
				this.dimensions = dimensions;
				this.cardCount = cardCount;
				this.cardTypeCount = cardTypeCount;
				this.moveCount = moveCount;
				this.cardModifiers = cardModifiers;
				this.backgroundSprite = backgroundSprite;
				this.islandRects = islandRects;
				this.cameraOffset = cameraOffset;
				this.timePerMove = timePerMove;
				this.musics = musics;
			}

			public IslandsLevel MakeLevel ()
			{
				IslandsLevel level = IslandsLevel.MakeLevel(dimensions, cardCount, cardTypeCount, islandCount, moveCount, cardModifiers, islandRects);
				if (level != null)
				{
					level.enabled = false;
					for (int i = 0; i < level.cardGroups.Length; i ++)
					{
						CardGroup cardGroup = level.cardGroups[i];
						Island island = (Island) cardGroup;
						Rect islandRect = islandRects[i];
						Vector2 islandPosition = islandRect.min + IslandsLevel.cardSize / 2;
						island.trs.position = islandPosition;
					}
				}
				return level;
			}
		}

		[Serializable]
		public class IslandsLevelZone
		{
			public IslandsLevelEntry firstLevelEntry;
			public int starsRequiredToUnlockMe;
			public int levelCount = 10;
		}
	}
}