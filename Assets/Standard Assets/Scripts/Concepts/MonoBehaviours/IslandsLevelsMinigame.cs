using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using IslandsLevelZone = CardQuest.IslandsLevelsData.IslandsLevelZone;
using IslandsLevelEntry = CardQuest.IslandsLevelsData.IslandsLevelEntry;

namespace CardQuest
{
	[ExecuteInEditMode]
	public class IslandsLevelsMinigame : SingletonUpdateWhileEnabled<IslandsLevelsMinigame>
	{
		public static int startingLevelIndex = 0;
		public static int zoneEndLevelIndex = 10;
#if UNITY_EDITOR
		public bool update;
#endif
		public IslandsLevelsData islandsLevelsData;
		public float levelSeperation;
		public Button nextLevelButton;
		public _Text levelNameText;
		public _Text timeText;
		public _Text movesText;
		public Image backgroundImage;
		public GameObject[] statusMenuStarIconGos = new GameObject[0];
		public GameObject[] nextLevelScreenStarIconGos = new GameObject[0];
		public AudioSource musicSource;
		public SerializableDictionary<int, ColliderGroup> islandsCollidersDict = new SerializableDictionary<int, ColliderGroup>();
		[HideInInspector]
		protected int currentLevelIndex = -1;
		[HideInInspector]
		protected IslandsLevel[] islandsLevels = new IslandsLevel[0];
		bool isOverParTime;
		bool isOverMoveCount;
		Rect previousIslandsLevelBoundsRect = RectExtensions.NULL;
		Vector2 previousIslandsLevelPosition = VectorExtensions.NULL;
		float levelStartTime;
		float levelTime;
		IslandsLevelEntry currentLevelEntry;
		IslandsLevel currentLevel;
		int moveCount;
		int indexOfNextStarToLose;
		
#if UNITY_EDITOR
		void OnValidate ()
		{
			if (update)
			{
				update = false;
				List<IslandsLevelEntry> _islandsLevelEntries = new List<IslandsLevelEntry>();
				islandsCollidersDict.Init ();
				for (int i = 0; i < islandsCollidersDict.Count; i ++)
				{
					Collider2D[] islandsColliders = islandsCollidersDict[i].colliders;
					IslandsLevelsData.IslandsLevelZone levelZone = islandsLevelsData.levelZones[i];
					IslandsLevelEntry firstLevelEntry = levelZone.firstLevelEntry;
					firstLevelEntry.islandRects = new Rect[islandsColliders.Length];
					for (int i2 = 0; i2 < islandsColliders.Length; i2 ++)
						firstLevelEntry.islandRects[i2] = islandsColliders[i2].GetRect();
					for (int levelIndex = 0; levelIndex < levelZone.levelCount; levelIndex ++)
					{
						IslandsLevelEntry islandsLevelEntry = new IslandsLevelEntry(firstLevelEntry);
						islandsLevelEntry.moveCount += levelIndex;
						islandsLevelEntry.name += " " + (levelIndex + 1);
						islandsLevelEntry.timePerMove = islandsLevelsData.islandsLevelEntries[_islandsLevelEntries.Count].timePerMove;
						_islandsLevelEntries.Add(islandsLevelEntry);
					}
				}
				islandsLevelsData.islandsLevelEntries = _islandsLevelEntries.ToArray();
			}
		}
#endif

		public virtual IEnumerator Start ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				yield break;
#endif
			islandsLevels = new IslandsLevel[islandsLevelsData.islandsLevelEntries.Length];
			currentLevelIndex = startingLevelIndex;
			yield return StartCoroutine(GoToNextLevelRoutine ());
			// StartCoroutine(MakeLevelsRoutine ());
			if (currentLevelEntry != null && currentLevelEntry.musics != null && currentLevelEntry.musics.Length > 0)
				StartCoroutine(PlayMusics ());
		}

		public override void DoUpdate ()
		{
			levelTime = Time.timeSinceLevelLoad - levelStartTime;
			if (currentLevelEntry == null)
				return;
			timeText.text.text = levelTime.ToString("F1") + " / " + (currentLevelEntry.timePerMove * currentLevel.movesRequiredToWin).ToString("F1");
			if (!isOverParTime && levelTime > currentLevelEntry.timePerMove * currentLevelEntry.moveCount)
			{
				isOverParTime = true;
				statusMenuStarIconGos[indexOfNextStarToLose].SetActive(false);
				nextLevelScreenStarIconGos[indexOfNextStarToLose].SetActive(false);
				indexOfNextStarToLose ++;
			}
		}

		IEnumerator MakeNextLevelRoutine (IslandsLevelEntry islandsLevelEntry)
		{
			Rect islandsLevelBoundsRect;
			// IslandsLevel islandsLevel;
			// do
			// {
			// 	islandsLevel = islandsLevelEntry.MakeLevel();
			// 	if (islandsLevel != null)
			// 	{
			// 		islandsLevels[latestLevelIndex] = islandsLevel;
			// 		if (HasEquivalentLevel(latestLevelIndex))
			// 			DestroyImmediate(islandsLevel.gameObject);
			// 	}
			// 	yield return new WaitForEndOfFrame();
			// } while (islandsLevel == null);
			IslandsLevel islandsLevel = null;
			while (islandsLevel == null)
			{
				if (LevelCacher.instance.targetCacheCountsForLevelIndicesDict.keys[0] <= currentLevelIndex)
					islandsLevel = LevelCacher.instance.GetLevel(currentLevelIndex);
				else
					islandsLevel = islandsLevelEntry.MakeLevel();
				islandsLevels[currentLevelIndex] = islandsLevel;
				yield return new WaitForEndOfFrame();
			}
			islandsLevel.gameObject.SetActive(true);
			List<Rect> cardSlotRects = new List<Rect>();
			foreach (CardGroup cardGroup in islandsLevel.cardGroups)
			{
				Island island = (Island) cardGroup;
				foreach (Card card in island.cards)
					card.gameObject.layer = 0;
				foreach (CardSlot cardSlot in island.cardSlots)
				{
					cardSlot.gameObject.layer = 0;
					cardSlotRects.Add(cardSlot.spriteRenderer.bounds.ToRect());
				}
			}
			islandsLevelBoundsRect = RectExtensions.Combine(cardSlotRects.ToArray());
			if (previousIslandsLevelPosition != (Vector2) VectorExtensions.NULL)
				islandsLevel.trs.position = previousIslandsLevelPosition + (Vector2.right * (previousIslandsLevelBoundsRect.size.x / 2 + islandsLevelBoundsRect.size.x / 2 + levelSeperation));
			previousIslandsLevelBoundsRect = islandsLevelBoundsRect;
			previousIslandsLevelPosition = islandsLevel.trs.position;
			islandsLevel.name = islandsLevelEntry.name;
			nextLevelButton.interactable = true;
		}

		public virtual void GoToNextLevel ()
		{
			if (currentLevelIndex == zoneEndLevelIndex)
			{
				GameManager.instance.LoadScene ("World");
				return;
			}
			Destroy(currentLevel.gameObject);
			currentLevelIndex ++;
			nextLevelButton.gameObject.SetActive(false);
			StartCoroutine(GoToNextLevelRoutine ());
		}

		IEnumerator GoToNextLevelRoutine ()
		{
			yield return StartCoroutine(MakeNextLevelRoutine (islandsLevelsData.islandsLevelEntries[currentLevelIndex]));
			GoToLevel (currentLevelIndex);
		}

		protected void GoToLevel (int levelIndex)
		{
			currentLevel = islandsLevels[levelIndex];
			currentLevel.enabled = true;
			currentLevelEntry = islandsLevelsData.islandsLevelEntries[levelIndex];
			backgroundImage.sprite = currentLevelEntry.backgroundSprite;
			backgroundImage.enabled = true;
			moveCount = 0;
			movesText.text.text = "0 / " + currentLevel.movesRequiredToWin;
			levelNameText.text.text = currentLevel.name;
			foreach (CardGroup cardGroup in currentLevel.cardGroups)
			{
				Island island = (Island) cardGroup;
				foreach (CardSlot cardSlot in island.cardSlots)
					cardSlot.collider.isTrigger = false;
				foreach (Card card in island.cards)
					card.collider.isTrigger = false;
			}
			indexOfNextStarToLose = 0;
			foreach (GameObject starIconGo in statusMenuStarIconGos)
				starIconGo.SetActive(true);
			foreach (GameObject starIconGo in nextLevelScreenStarIconGos)
				starIconGo.SetActive(true);
			ShowLevel (currentLevel, currentLevelEntry);
			levelStartTime = Time.timeSinceLevelLoad;
			enabled = true;
		}

		void ShowLevel (int levelIndex)
		{
			ShowLevel (islandsLevels[levelIndex], islandsLevelsData.islandsLevelEntries[levelIndex]);
		}

		void ShowLevel (IslandsLevel islandsLevel, IslandsLevelEntry islandsLevelEntry)
		{
			List<Rect> cardSlotRects = new List<Rect>();
			foreach (CardGroup cardGroup in islandsLevel.cardGroups)
			{
				Island island = (Island) cardGroup;
				foreach (CardSlot cardSlot in island.cardSlots)
					cardSlotRects.Add(cardSlot.spriteRenderer.bounds.ToRect());
			}
			Rect viewRect = RectExtensions.Combine(cardSlotRects.ToArray());
			CameraScript.instance.trs.position = (viewRect.center + islandsLevelEntry.cameraOffset).SetZ(CameraScript.instance.trs.position.z);
		}

		public void OnLevelComplete (IslandsLevel level)
		{
			enabled = false;
			int stars = 1;
			IslandsLevelEntry levelEntry = null;
			for (int i = 0; i < islandsLevelsData.islandsLevelEntries.Length; i ++)
			{
				levelEntry = islandsLevelsData.islandsLevelEntries[i];
				if (levelEntry.name == level.name)
					break;
			}
			if (levelTime <= levelEntry.timePerMove * level.movesRequiredToWin)
				stars ++;
			if (moveCount <= level.movesRequiredToWin)
				stars ++;
			int previousStars = GetLevelStars(level.name);
			if (stars > previousStars)
			{
				GameManager.Stars += stars - previousStars;
				SetLevelStars (level.name, stars);
			}
			SetLevelCompleted (level.name, true);
			nextLevelButton.gameObject.SetActive(true);
		}

		public void Restart ()
		{
			if (currentLevel != null)
				DestroyImmediate(currentLevel.gameObject);
			startingLevelIndex = currentLevelIndex;
			GameManager.instance.ReloadActiveScene ();
		}

		public void OnMoveMade ()
		{
			moveCount ++;
			movesText.text.text = "" + moveCount + " / " + currentLevel.movesRequiredToWin;
			if (!isOverMoveCount && moveCount > currentLevelEntry.moveCount)
			{
				isOverMoveCount = true;
				statusMenuStarIconGos[indexOfNextStarToLose].SetActive(false);
				nextLevelScreenStarIconGos[indexOfNextStarToLose].SetActive(false);
				indexOfNextStarToLose ++;
			}
		}

		IEnumerator PlayMusics ()
		{
			do
			{
				AudioClip music;
				do
				{
					music = currentLevelEntry.musics[Random.Range(0, currentLevelEntry.musics.Length)];
				} while (musicSource.clip == music);
				musicSource.clip = music;
				musicSource.Play();
				yield return new WaitUntil(() => (!musicSource.isPlaying));
			} while (true);
		}

		public static bool GetLevelCompleted (string levelName)
		{
			return PlayerPrefs.GetInt(levelName + " completed", 0) == 1;
		}

		public static void SetLevelCompleted (string levelName, bool completed)
		{
			PlayerPrefs.SetInt(levelName + " completed", completed.GetHashCode());
		}

		public static int GetLevelStars (string levelName)
		{
			return PlayerPrefs.GetInt(levelName + " stars", 0);
		}

		public static void SetLevelStars (string levelName, int stars)
		{
			PlayerPrefs.SetInt(levelName + " stars", stars);
		}

		[Serializable]
		public class ColliderGroup
		{
			public Collider2D[] colliders = new Collider2D[0];
		}
	}
}