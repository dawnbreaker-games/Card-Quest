using UnityEngine;
using UnityEngine.UI;

namespace CardQuest
{
	[RequireComponent(typeof(Button))]
	public class _Button : MonoBehaviour
	{
		public Button button;

		void OnEnable ()
		{
			button.onClick.AddListener(MakeSound);
		}

		void OnDisable ()
		{
			button.onClick.RemoveListener(MakeSound);
		}

		void MakeSound ()
		{
			AudioManager.instance.PlaySoundEffect ();
		}
	}
}