using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CardQuest
{
	public class OptionsMenu : MonoBehaviour
	{
		public float Volume
		{
			get
			{
				return PlayerPrefs.GetFloat("Volume", 1);
			}
			set
			{
				PlayerPrefs.SetFloat("Volume", value);
			}
		}
		public bool Mute
		{
			get
			{
				return PlayerPrefs.GetInt("Mute", 0) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("Mute", value.GetHashCode());
			}
		}
		public Slider volumeSlider;
		public Toggle muteToggle;
		public Button[] viewCutscenesButtons = new Button[0];

		void Awake ()
		{
			volumeSlider.value = Volume;
			muteToggle.isOn = Mute;
			for (int i = 0; i < CutsceneManager.UnlockedCutsceneCount; i ++)
			{
				Button viewCutsceneButton = viewCutscenesButtons[i];
				viewCutsceneButton.interactable = true;
			}
		}

		public void SetVolume (float volume)
		{
			AudioManager.instance.SetVolume (volume);
			Volume = volume;
		}

		public void SetMute (bool mute)
		{
			AudioManager.instance.SetMute (mute);
			Mute = mute;
		}

		public void ClearData ()
		{
			PlayerPrefs.DeleteAll();
			GameManager.instance.LoadScene (0);
		}
	}
}