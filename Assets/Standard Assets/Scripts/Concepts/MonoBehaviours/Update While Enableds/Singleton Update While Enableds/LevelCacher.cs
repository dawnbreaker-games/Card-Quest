using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class LevelCacher : SingletonUpdateWhileEnabled<LevelCacher>
	{
		public string LevelsData
		{
			get
			{
				return PlayerPrefs.GetString("Levels data", "");
			}
			set
			{
				PlayerPrefs.SetString("Levels data", value);
			}
		}
		public SerializableDictionary<int, int> targetCacheCountsForLevelIndicesDict = new SerializableDictionary<int, int>();
		public IslandsLevelsData levelsData;
		Dictionary<int, List<IslandsLevel>> levelsAndIndicesDict = new Dictionary<int, List<IslandsLevel>>();

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			targetCacheCountsForLevelIndicesDict.Init ();
			for (int i = 0; i < targetCacheCountsForLevelIndicesDict.Count; i ++)
			{
				int levelIndex = targetCacheCountsForLevelIndicesDict.keys[i];
				levelsAndIndicesDict.Add(levelIndex, new List<IslandsLevel>());
			}
			string[] levelIndicesAndDatas = LevelsData.Split(new string[] { GameManager.STRING_SEPERATOR }, StringSplitOptions.RemoveEmptyEntries);
			for (int i = 0; i < levelIndicesAndDatas.Length; i += 2)
			{
				int levelIndex = int.Parse(levelIndicesAndDatas[i]);
				List<IslandsLevel> levels;
				IslandsLevel level = IslandsLevel.FromString(levelsData.islandsLevelEntries[levelIndex], levelIndicesAndDatas[i + 1]);
				DontDestroyOnLoad(level);
				if (levelsAndIndicesDict.TryGetValue(levelIndex, out levels))
				{
					levels.Add(level);
					levelsAndIndicesDict[levelIndex] = levels;
				}
			}
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < targetCacheCountsForLevelIndicesDict.Count; i ++)
			{
				int levelIndex = targetCacheCountsForLevelIndicesDict.keys[i];
				int cacheCount = levelsAndIndicesDict[levelIndex].Count;
				int targetCacheCount = targetCacheCountsForLevelIndicesDict[levelIndex];
				if (cacheCount < targetCacheCount)
				{
					IslandsLevelsData.IslandsLevelEntry levelEntry = levelsData.islandsLevelEntries[levelIndex];
					IslandsLevel level = TryToMakeLevel(levelEntry);
					if (level != null)
					{
						LevelsData += levelIndex + GameManager.STRING_SEPERATOR + level.ToString(levelEntry) + GameManager.STRING_SEPERATOR;
						List<IslandsLevel> levels;
						if (levelsAndIndicesDict.TryGetValue(levelIndex, out levels))
						{
							levels.Add(level);
							levelsAndIndicesDict[levelIndex] = levels;
						}
					}
				}
			}
		}

		IslandsLevel TryToMakeLevel (IslandsLevelsData.IslandsLevelEntry levelEntry, int maxRetries = 1)
		{
			for (int i = 0; i < maxRetries; i ++)
			{
				IslandsLevel level = levelEntry.MakeLevel();
				if (level != null)
				{
					DontDestroyOnLoad(level);
					return level;
				}
			}
			return null;
		}

		public IslandsLevel GetLevel (int levelIndex)
		{
			IslandsLevel output = null;
			List<IslandsLevel> levels;
			if (levelsAndIndicesDict.TryGetValue(levelIndex, out levels) && levels.Count > 0)
			{
				output = levels[0];
				levels.RemoveAt(0);
				string[] levelIndicesAndDatas = LevelsData.Split(new string[] { GameManager.STRING_SEPERATOR }, StringSplitOptions.RemoveEmptyEntries);
				for (int i = 0; i < levelIndicesAndDatas.Length; i += 2)
				{
					int _levelIndex = int.Parse(levelIndicesAndDatas[i]);
					if (_levelIndex == levelIndex)
					{
						LevelsData = LevelsData.Replace(levelIndex + GameManager.STRING_SEPERATOR + levelIndicesAndDatas[i + 1] + GameManager.STRING_SEPERATOR, "");
						return output;
					}
				}
				levelsAndIndicesDict[levelIndex] = levels;
			}
			return output;
		}
	}
}