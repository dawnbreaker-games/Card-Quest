using Extensions;
using UnityEngine;

namespace CardQuest
{
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public Card[] cards = new Card[0];
		public CardGroup[] cardGroups = new CardGroup[0];
	}
}