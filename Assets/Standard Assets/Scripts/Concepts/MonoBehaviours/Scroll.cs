﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class Scroll : MonoBehaviour
	{
		public Transform trs;
		[Multiline(15)]
		public string text;
	}
}