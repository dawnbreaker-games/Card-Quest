using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace CardQuest
{
	public class IslandsLevel : Level
	{
		public static Vector2 cardSize;
		public Card selectedCard;
		public Card highlightedCard;
		public Transform selectedCardIndicatorTrs;
		public Transform highlightedCardIndicatorTrs;
		public Transform trs;
		public int movesRequiredToWin;
		public AudioClip[] winLevelSounds = new AudioClip[0];
		public AudioClip[] moveCardSounds = new AudioClip[0];
		[HideInInspector]
		public CardModifier[] cardModifiers = new CardModifier[0];
		bool previousLeftMouseButtonInput;
		bool leftMouseButtonInput;

		public override void DoUpdate ()
		{
			base.DoUpdate ();
			leftMouseButtonInput = InputManager.LeftClickInput;
			HandleMouseInput ();
			previousLeftMouseButtonInput = leftMouseButtonInput;
		}

		void HandleMouseInput ()
		{
			Collider2D hitCollider = Physics2D.OverlapPoint(CameraScript.instance.camera.ScreenToWorldPoint(InputManager.MousePosition));
			if (hitCollider != null)
			{
				highlightedCard = hitCollider.GetComponent<Card>();
				highlightedCardIndicatorTrs.SetParent(highlightedCard.trs);
				highlightedCardIndicatorTrs.localScale = Vector3.one;
				highlightedCardIndicatorTrs.localPosition = Vector3.zero;
				highlightedCardIndicatorTrs.gameObject.SetActive(true);
				if (leftMouseButtonInput && !previousLeftMouseButtonInput)
				{
					if (selectedCard != null && highlightedCard is CardSlot)
					{
						if (TryToMoveSelectedCardToHighlightedPosition ())
							IslandsLevelsMinigame.instance.OnMoveMade ();
						else
						{
							selectedCard = highlightedCard;
							selectedCardIndicatorTrs.SetParent(selectedCard.trs);
							selectedCardIndicatorTrs.localScale = Vector3.one;
							selectedCardIndicatorTrs.localPosition = Vector3.zero;
							selectedCardIndicatorTrs.gameObject.SetActive(true);
						}
						if (IsLevelCompleted())
						{
							AudioClip winLevelSound = winLevelSounds[Random.Range(0, winLevelSounds.Length)];
							AudioManager.instance.PlaySoundEffect (null, new SoundEffect.Settings(winLevelSound));
							IslandsLevelsMinigame.instance.OnLevelComplete (this);
						}
					}
					else
					{
						selectedCard = highlightedCard;
						selectedCardIndicatorTrs.SetParent(selectedCard.trs);
						selectedCardIndicatorTrs.localScale = Vector3.one;
						selectedCardIndicatorTrs.localPosition = Vector3.zero;
						selectedCardIndicatorTrs.gameObject.SetActive(true);
					}
				}
			}
			else
			{
				highlightedCardIndicatorTrs.gameObject.SetActive(false);
				if (leftMouseButtonInput && !previousLeftMouseButtonInput)
				{
					selectedCard = null;
					selectedCardIndicatorTrs.gameObject.SetActive(false);
				}
			}
		}

		bool IsLevelCompleted ()
		{
			Dictionary<Vector2Int, string> cardTypePositions = new Dictionary<Vector2Int, string>();
			for (int i = 0; i < cardGroups.Length; i ++)
			{
				Island island = (Island) cardGroups[i];
				foreach (CardSlot cardSlot in island.cardSlots)
				{
					if (cardSlot.cardAboveMe != null)
					{
						if (i == 0)
							cardTypePositions.Add(cardSlot.position, cardSlot.cardAboveMe.type);
						else
						{
							string cardType;
							if (cardTypePositions.TryGetValue(cardSlot.position, out cardType) && cardType == cardSlot.cardAboveMe.type)
							{
							}
							else
								return false;
						}
					}
					else if (i > 0 && cardTypePositions.ContainsKey(cardSlot.position))
						return false;
				}
			}
			return true;
		}

		int GetMatchCount ()
		{
			int output = 0;
			Dictionary<Vector2Int, string> cardTypePositions = new Dictionary<Vector2Int, string>();
			for (int i = 0; i < cardGroups.Length; i ++)
			{
				Island island = (Island) cardGroups[i];
				foreach (CardSlot cardSlot in island.cardSlots)
				{
					if (cardSlot.cardAboveMe != null)
					{
						if (i == 0)
							cardTypePositions.Add(cardSlot.position, cardSlot.cardAboveMe.type);
						else
						{
							string cardType;
							if (cardTypePositions.TryGetValue(cardSlot.position, out cardType) && cardType == cardSlot.cardAboveMe.type)
								output ++;
						}
					}
				}
			}
			return output;
		}

		bool TryToMoveSelectedCardToHighlightedPosition ()
		{
			Island highlighedCardIsland = (Island) highlightedCard.groupsIAmPartOf[0];
			Island selectedCardIsland = (Island) selectedCard.groupsIAmPartOf[0];
			if (highlighedCardIsland == selectedCardIsland)
				return false;
			bool isCardSlotMousedOver = false;
			foreach (Card cardSlot in highlighedCardIsland.cardSlots)
			{
				if (cardSlot.position == highlightedCard.position)
				{
					isCardSlotMousedOver = true;
					break;
				}
			}
			if (!isCardSlotMousedOver)
				return false;
			bool isNextToSameType = false;
			foreach (Card card in highlighedCardIsland.cards)
			{
				float distanceFromHighlighted = Vector2Int.Distance(card.position, highlightedCard.position);
				if (distanceFromHighlighted == 0)
					return false;
				else if (card.type == selectedCard.type && distanceFromHighlighted == 1)
				{
					isNextToSameType = true;
					break;
				}
			}
			if (!isNextToSameType)
				return false;
			MoveSelectedCardToHighlightedPosition ();
			AudioClip moveCardSound = moveCardSounds[Random.Range(0, moveCardSounds.Length)];
			AudioManager.instance.PlaySoundEffect (null, new SoundEffect.Settings(moveCardSound));
			return true;
		}

		void MoveSelectedCardToHighlightedPosition ()
		{
			Island highlighedIsland = (Island) highlightedCard.groupsIAmPartOf[0];
			CardGroup selectedIsland = selectedCard.groupsIAmPartOf[0];
			selectedCard.trs.position = highlightedCard.trs.position.SetZ(0);
			selectedCard.position = highlightedCard.position;
			highlighedIsland.cards = highlighedIsland.cards.Add(selectedCard);
			selectedCard.groupsIAmPartOf = new CardGroup[1] { highlighedIsland };
			selectedIsland.cards = selectedIsland.cards.Remove(selectedCard);
			CardSlot highlightedCardSlot = (CardSlot) highlightedCard;
			highlightedCardSlot.cardAboveMe = selectedCard;
			selectedCard.cardSlotUnderMe.cardAboveMe = null;
			selectedCard.cardSlotUnderMe = highlightedCardSlot;
			selectedCard.trs.SetParent(highlighedIsland.trs);
			for (int i = 0; i < cardModifiers.Length; i ++)
			{
				CardModifier cardModifier = cardModifiers[i];
				if (!cardModifier.onlyAffectAfterMove || highlightedCardSlot.cardAboveMe.cardModifiers.Contains(cardModifier))
					cardModifier.ApplyEffect ();
			}
		}

		public bool IsEquivalent (IslandsLevel otherLevel)
		{
			Dictionary<string, List<Vector2Int>> cardTypePositionsDict = new Dictionary<string, List<Vector2Int>>();
			foreach (CardGroup cardGroup in cardGroups)
			{
				foreach (Card card in cardGroup.cards)
				{
					if (!cardTypePositionsDict.ContainsKey(card.type))
					{
						List<Vector2Int> cardPositions = new List<Vector2Int>();
						cardPositions.Add(card.position);
						cardTypePositionsDict.Add(card.type, cardPositions);
					}
					else
						cardTypePositionsDict[card.type].Add(card.position);
				}
			}
			Dictionary<string, List<Vector2Int>> otherCardTypePositionsDict = new Dictionary<string, List<Vector2Int>>();
			foreach (CardGroup cardGroup in otherLevel.cardGroups)
			{
				foreach (Card card in cardGroup.cards)
				{
					if (!otherCardTypePositionsDict.ContainsKey(card.type))
					{
						List<Vector2Int> cardPositions = new List<Vector2Int>();
						cardPositions.Add(card.position);
						otherCardTypePositionsDict.Add(card.type, cardPositions);
					}
					else
						otherCardTypePositionsDict[card.type].Add(card.position);
				}
			}
			if (cardTypePositionsDict.Count != otherCardTypePositionsDict.Count)
				return false;
			List<Vector2Int>[] _cardTypePositions = new List<Vector2Int>[cardTypePositionsDict.Count];
			cardTypePositionsDict.Values.CopyTo(_cardTypePositions, 0);
			List<List<Vector2Int>> cardTypePositions = new List<List<Vector2Int>>();
			cardTypePositions = _cardTypePositions.ToList();
			List<Vector2Int>[] _otherCardTypePositions = new List<Vector2Int>[otherCardTypePositionsDict.Count];
			otherCardTypePositionsDict.Values.CopyTo(_otherCardTypePositions, 0);
			return false;
		}
		
		public static IslandsLevel MakeLevel (Vector2Int dimensions, int cardCount = 4, int cardTypeCount = 1, int islandCount = 2, int moveCount = 1, CardModifier[] cardModifiers = null, Rect[] islandRects = null)
		{
			IslandsLevel islandsLevel = Instantiate(GameManager.instance.islandsLevelPrefab);
			int cardsPerIsland = cardCount / islandCount;
			Island island = MakeIsland(islandsLevel, dimensions, cardsPerIsland, cardTypeCount);
			Rect islandRect = islandRects[0];
			island.trs.position = islandRect.min;
			Vector2 targetSize = islandRect.size / dimensions.Multiply(cardSize);
			island.trs.localScale = Vector3.one * Mathf.Min(targetSize.x, targetSize.y);
			islandsLevel.cardGroups = new CardGroup[islandCount];
			islandsLevel.cardGroups[0] = island;
			List<Card> cards = new List<Card>(island.cards);
			for (int i = 1; i < islandCount; i ++)
			{
				island = Instantiate(island, islandsLevel.trs);
				islandsLevel.cardGroups[i] = island;
				islandRect = islandRects[i];
				island.trs.position = islandRect.min;
				cards.AddRange(island.cards);
			}
			islandsLevel.cards = cards.ToArray();
			for (int i = 0; i < cardModifiers.Length; i ++)
			{
				CardModifier cardModifier = Instantiate(cardModifiers[i], islandsLevel.trs);
				List<Card> cardsRemaining = new List<Card>(cards);
				while (cardsRemaining.Count > 0)
				{
					int randomIndex = Random.Range(0, cardsRemaining.Count);
					Card card = cardsRemaining[randomIndex];
					if (cardModifier.CanAttach(card))
					{
						cardModifier.Attach (card);
						islandsLevel.cardModifiers = islandsLevel.cardModifiers.Add(cardModifier);
						break;
					}
					cardsRemaining.RemoveAt(randomIndex);
				}
				if (cardModifier.affectedCard == null)
				{
					DestroyImmediate(islandsLevel.gameObject);
					return null;
				}
			}
			if (!MakeMoves(islandsLevel, moveCount))
			{
				DestroyImmediate(islandsLevel.gameObject);
				return null;
			}
			islandsLevel.selectedCard = null;
			islandsLevel.highlightedCard = null;
			return islandsLevel;
		}

		static Island MakeIsland (IslandsLevel islandsLevel, Vector2Int dimensions, int cardCount = 2, int cardTypeCount = 1)
		{
			Island island = Instantiate(GameManager.instance.islandPrefab, islandsLevel.trs);
			List<Card> notUsedIslandCardPrefabs = new List<Card>(GameManager.instance.islandsLevelCardPrefabs);
			cardSize = notUsedIslandCardPrefabs[0].spriteRenderer.bounds.ToRect().size;
			List<Vector2Int> cardPositions = new List<Vector2Int>();
			List<Vector2Int> possibleNextCardPositions = new List<Vector2Int>();
			List<Card> cards = new List<Card>();
			List<CardSlot> cardSlots = new List<CardSlot>();
			for (int y = 0; y < dimensions.y; y ++)
			{
				for (int x = 0; x < dimensions.x; x ++)
				{
					CardSlot cardSlot = Instantiate(GameManager.instance.cardSlotPrefab, island.trs);
					Vector2Int cardPosition = new Vector2Int(x, y);
					cardSlot.position = cardPosition;
					cardSlot.trs.localPosition = (cardPosition.Multiply(cardSize) + cardSize / 2).SetZ(1);
					cardSlot.groupsIAmPartOf = new CardGroup[1] { island };
					cardSlots.Add(cardSlot);
					possibleNextCardPositions.Add(cardPosition);
					island.cardSlotPositionsDict.Add(cardSlot.position, cardSlot);
				}
			}
			island.cardSlots = cardSlots.ToArray();
			for (int i = 0; i < cardTypeCount; i ++)
			{
				int notUsedIslandCardPrefabIndex = Random.Range(0, notUsedIslandCardPrefabs.Count);
				for (int i2 = 0; i2 < cardCount / cardTypeCount; i2 ++)
				{
					Card card = Instantiate(notUsedIslandCardPrefabs[notUsedIslandCardPrefabIndex], island.trs);
					int indexOfCardPosition = Random.Range(0, possibleNextCardPositions.Count);
					Vector2Int cardPosition = possibleNextCardPositions[indexOfCardPosition];
					possibleNextCardPositions.RemoveAt(indexOfCardPosition);
					card.trs.localPosition = cardPosition.Multiply(cardSize) + cardSize / 2;
					card.position = cardPosition;
					CardSlot cardSlot = island.cardSlotPositionsDict[cardPosition];
					card.cardSlotUnderMe = cardSlot;
					card.groupsIAmPartOf = new CardGroup[] { island };
					cardSlot.cardAboveMe = card;
					cards.Add(card);
				}
				notUsedIslandCardPrefabs.RemoveAt(notUsedIslandCardPrefabIndex);
			}
			island.cards = cards.ToArray();
			return island;
		}

		static bool MakeMoves (IslandsLevel islandsLevel, int moveCount = 1, int maxRetries = 50)
		{
			int matchCount = islandsLevel.GetMatchCount();
			int previousMatchCount = matchCount;
			for (int i = 0; i < moveCount; i ++)
			{
				for (int i2 = 0; i2 < maxRetries; i2 ++)
				{
					Card cardToMove;
					List<CardGroup> remainingCardGroups = new List<CardGroup>(islandsLevel.cardGroups);
					int selectedIslandIndex = Random.Range(0, remainingCardGroups.Count);
					Island selectedIsland = (Island) remainingCardGroups[selectedIslandIndex];
					remainingCardGroups.RemoveAt(selectedIslandIndex);
					List<Card> possibleCardsToMove = new List<Card>();
					possibleCardsToMove.AddRange(selectedIsland.cards);
					while (true)
					{
						int indexOfCardToMove = Random.Range(0, possibleCardsToMove.Count);
						cardToMove = possibleCardsToMove[indexOfCardToMove];
						possibleCardsToMove.RemoveAt(indexOfCardToMove);
						if (IsValidEndOfMove(cardToMove))
							break;
						else if (possibleCardsToMove.Count == 0)
						{
							if (remainingCardGroups.Count == 0)
								return false;
							selectedIslandIndex = Random.Range(0, remainingCardGroups.Count);
							selectedIsland = (Island) remainingCardGroups[selectedIslandIndex];
							remainingCardGroups.RemoveAt(selectedIslandIndex);
							possibleCardsToMove.Clear();
							possibleCardsToMove.AddRange(selectedIsland.cards);
						}
					}
					remainingCardGroups.Clear();
					remainingCardGroups.AddRange(islandsLevel.cardGroups);
					remainingCardGroups.RemoveAt(selectedIslandIndex);
					int highlightedCardIndex = Random.Range(0, remainingCardGroups.Count);
					Island highlightedIsland = (Island) remainingCardGroups[highlightedCardIndex];
					List<CardSlot> possibleCardSlotsToMoveTo = new List<CardSlot>();
					possibleCardSlotsToMoveTo.AddRange(highlightedIsland.cardSlots);
					foreach (Card card in highlightedIsland.cards)
						possibleCardSlotsToMoveTo.Remove(card.cardSlotUnderMe);
					int indexOfCardSlotToMoveTo = Random.Range(0, possibleCardSlotsToMoveTo.Count);
					CardSlot cardSlotToMoveFrom = cardToMove.cardSlotUnderMe;
					CardSlot cardSlotToMoveTo = possibleCardSlotsToMoveTo[indexOfCardSlotToMoveTo];
					islandsLevel.selectedCard = cardToMove;
					islandsLevel.highlightedCard = cardSlotToMoveTo;
					islandsLevel.MoveSelectedCardToHighlightedPosition ();
					matchCount = islandsLevel.GetMatchCount();
					if (matchCount == 0)
					{
						int raiseMoveCount = RaiseMoveCount(cardToMove, cardSlotToMoveTo);
						islandsLevel.movesRequiredToWin = i + 1 + raiseMoveCount;
						return true;
					}
					else if (matchCount >= previousMatchCount)
					{
						if (i2 == maxRetries - 1)
							return false;
						islandsLevel.selectedCard = cardToMove;
						islandsLevel.highlightedCard = cardSlotToMoveFrom;
						islandsLevel.MoveSelectedCardToHighlightedPosition ();
					}
					else
					{
						int raiseMoveCount = RaiseMoveCount(cardToMove, cardSlotToMoveTo);
						i += raiseMoveCount;
						moveCount += raiseMoveCount;
						break;
					}
				}
				previousMatchCount = matchCount;
			}
			islandsLevel.movesRequiredToWin = moveCount;
			return true;
		}

		static bool IsValidEndOfMove (Card card)
		{
			Card[] otherCards = card.groupsIAmPartOf[0].cards;
			for (int i = 0; i < otherCards.Length; i ++)
			{
				Card otherCard = otherCards[i];
				if (Vector2Int.Distance(card.position, otherCard.position) == 1)
				{
					if (card.type == otherCard.type && card.GetModifier<RotateCardModifier>() == null)
						return true;
					else if (card.type.Replace(RotateCardModifier.ROTATED_TYPE_INDICATOR, "") == otherCard.type.Replace(RotateCardModifier.ROTATED_TYPE_INDICATOR, "") && card.type.EndsWith(RotateCardModifier.ROTATED_TYPE_INDICATOR) != otherCard.type.EndsWith(RotateCardModifier.ROTATED_TYPE_INDICATOR))
						return true;
				}
			}
			return false;
		}

		static int RaiseMoveCount (Card card, CardSlot cardSlotMovedFrom)
		{
			if (card.GetModifier<RotateCardModifier>() == null)
				return 0;
			int output = 0;
			Card[] otherCards = cardSlotMovedFrom.groupsIAmPartOf[0].cards;
			for (int i = 0; i < otherCards.Length; i ++)
			{
				Card otherCard = otherCards[i];
				if (Vector2Int.Distance(cardSlotMovedFrom.position, otherCard.position) == 1 && otherCard.GetModifier<RotateCardModifier>() != null)
				{
					if (card.type != otherCard.type)
						output = 1;
					else
						return 0;
				}
			}
			return output;
		}

		public string ToString (IslandsLevelsData.IslandsLevelEntry levelEntry)
		{
			string output = "";
			List<Card> cardsRemaining = new List<Card>();
			Card[] cards = new Card[levelEntry.cardCount];
			int currentCardIndex = 0;
			for (int i = 0; i < cardGroups.Length; i ++)
			{
				CardGroup cardGroup = cardGroups[i];
				for (int i2 = 0; i2 < cardGroup.cards.Length; i2 ++)
				{
					Card card = cardGroup.cards[i2];
					cardsRemaining.Add(card);
					cards[currentCardIndex] = card;
					currentCardIndex ++;
				}
			}
			cardsRemaining.Sort((Card card, Card card2) => { return card.type.CompareTo(card2.type); });
			while (cardsRemaining.Count > 0)
			{
				Card card = cardsRemaining[0];
				int positionIndex = card.position.y * levelEntry.dimensions.x + card.position.x;
				positionIndex += levelEntry.dimensions.x * levelEntry.dimensions.y * cardGroups.IndexOf(card.groupsIAmPartOf[0]);
				output += "" + positionIndex + GameManager.STRING_SEPERATOR_2;
				cardsRemaining.RemoveAt(0);
			}
			for (int i = 0; i < cardModifiers.Length; i ++)
			{
				CardModifier cardModifier = cardModifiers[i];
				output += cards.IndexOf(cardModifier.affectedCard) + GameManager.STRING_SEPERATOR_2;
			}
			return output;
		}

		public static IslandsLevel FromString (IslandsLevelsData.IslandsLevelEntry levelEntry, string data)
		{
			IslandsLevel islandsLevel = Instantiate(GameManager.Instance.islandsLevelPrefab);
			int cardsPerIsland = levelEntry.cardCount / levelEntry.islandCount;
			Island island = Instantiate(GameManager.instance.islandPrefab, islandsLevel.trs);
			List<Card> notUsedIslandCardPrefabs = new List<Card>(GameManager.instance.islandsLevelCardPrefabs);
			cardSize = notUsedIslandCardPrefabs[0].spriteRenderer.bounds.ToRect().size;
			CardSlot[] allCardSlots = new CardSlot[levelEntry.dimensions.x * levelEntry.dimensions.y * levelEntry.islandCount];
			List<Card> cards = new List<Card>();
			List<CardSlot> cardSlots = new List<CardSlot>();
			int cardSlotIndex = 0;
			for (int y = 0; y < levelEntry.dimensions.y; y ++)
			{
				for (int x = 0; x < levelEntry.dimensions.x; x ++)
				{
					CardSlot cardSlot = Instantiate(GameManager.instance.cardSlotPrefab, island.trs);
					Vector2Int cardPosition = new Vector2Int(x, y);
					cardSlot.position = cardPosition;
					cardSlot.trs.localPosition = (cardPosition.Multiply(cardSize) + cardSize / 2).SetZ(1);
					cardSlot.groupsIAmPartOf = new CardGroup[1] { island };
					cardSlots.Add(cardSlot);
					allCardSlots[cardSlotIndex] = cardSlot;
					cardSlotIndex ++;
					island.cardSlotPositionsDict.Add(cardSlot.position, cardSlot);
				}
			}
			island.cardSlots = cardSlots.ToArray();
			Rect islandRect = levelEntry.islandRects[0];
			island.trs.position = islandRect.min;
			Vector2 targetSize = islandRect.size / levelEntry.dimensions.Multiply(cardSize);
			island.trs.localScale = Vector3.one * Mathf.Min(targetSize.x, targetSize.y);
			islandsLevel.cardGroups = new CardGroup[levelEntry.islandCount];
			islandsLevel.cardGroups[0] = island;
			Island[] islands = new Island[levelEntry.islandCount];
			islands[0] = island;
			for (int i = 1; i < levelEntry.islandCount; i ++)
			{
				island = Instantiate(island, islandsLevel.trs);
				islands[i] = island;
				for (int y = 0; y < levelEntry.dimensions.y; y ++)
				{
					for (int x = 0; x < levelEntry.dimensions.x; x ++)
					{
						allCardSlots[cardSlotIndex] = island.cardSlots[y * levelEntry.dimensions.x + x];
						cardSlotIndex ++;
					}
				}
				islandsLevel.cardGroups[i] = island;
				islandRect = levelEntry.islandRects[i];
				island.trs.position = islandRect.min;
			}
			for (int i = 0; i < levelEntry.cardTypeCount; i ++)
			{
				int notUsedIslandCardPrefabIndex = Random.Range(0, notUsedIslandCardPrefabs.Count);
				for (int i2 = 0; i2 < levelEntry.cardCount / levelEntry.cardTypeCount; i2 ++)
				{
					int indexOfStringSepeartor2 = data.IndexOf(GameManager.STRING_SEPERATOR_2);
					string dataPiece = data.Substring(0, indexOfStringSepeartor2);
					data = data.Substring(indexOfStringSepeartor2 + GameManager.STRING_SEPERATOR_2.Length);
					int indexOfCardPosition = int.Parse(dataPiece);
					CardSlot cardSlot = allCardSlots[indexOfCardPosition];
					Card card = Instantiate(notUsedIslandCardPrefabs[notUsedIslandCardPrefabIndex], cardSlot.trs.parent);
					Vector2Int cardPosition = cardSlot.position;
					card.trs.localPosition = cardPosition.Multiply(cardSize) + cardSize / 2;
					card.position = cardPosition;
					card.cardSlotUnderMe = cardSlot;
					card.groupsIAmPartOf = new CardGroup[] { island };
					cardSlot.cardAboveMe = card;
					cards.Add(card);
				}
				notUsedIslandCardPrefabs.RemoveAt(notUsedIslandCardPrefabIndex);
			}
			islandsLevel.cardModifiers = new CardModifier[levelEntry.cardModifiers.Length];
			for (int i = 0; i < levelEntry.cardModifiers.Length; i ++)
			{
				CardModifier cardModifierPrefab = levelEntry.cardModifiers[i];
				int indexOfStringSepeartor2 = data.IndexOf(GameManager.STRING_SEPERATOR_2);
				string dataPiece = data.Substring(0, indexOfStringSepeartor2);
				data = data.Substring(indexOfStringSepeartor2 + GameManager.STRING_SEPERATOR_2.Length);
				int indexOfCard = int.Parse(dataPiece);
				CardModifier cardModifier = Instantiate(cardModifierPrefab);
				cardModifier.Attach (cards[indexOfCard]);
				islandsLevel.cardModifiers[i] = cardModifier;
			}
			island.cards = cards.ToArray();
			return islandsLevel;
		}
	}
}