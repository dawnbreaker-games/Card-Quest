using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace CardQuest
{
	public class RotateCardModifier : CardModifier
	{
		public const string ROTATED_TYPE_INDICATOR = " Rotated";

		public override void ApplyEffect ()
		{
			if (affectedCard.type.EndsWith(ROTATED_TYPE_INDICATOR))
				affectedCard.type = affectedCard.type.Remove(affectedCard.type.Length - ROTATED_TYPE_INDICATOR.Length);
			else
				affectedCard.type += ROTATED_TYPE_INDICATOR;
			affectedCard.trs.eulerAngles += Vector3.forward * 180;
		}

		public override bool CanAttach (Card card)
		{
			if (cardTypesAndTypeIdsMustCorrespond)
			{
				IslandsLevel islandsLevel = card.trs.parent.parent.GetComponent<IslandsLevel>();
				List<CardModifier> otherInstances = new List<CardModifier>(islandsLevel.GetComponentsInChildren<CardModifier>(true));
				otherInstances.Remove(this);
				for (int i = 0; i < otherInstances.Count; i ++)
				{
					CardModifier cardModifier = otherInstances[i];
					if ((cardModifier.typeId == typeId) != (cardModifier.affectedCard.type.Replace(ROTATED_TYPE_INDICATOR, "") == card.type.Replace(ROTATED_TYPE_INDICATOR, "")))
						return false;
				}
			}
			bool previousCardTypesAndTypeIdsMustCorrespond = cardTypesAndTypeIdsMustCorrespond;
			cardTypesAndTypeIdsMustCorrespond = false;
			bool output = base.CanAttach(card);
			cardTypesAndTypeIdsMustCorrespond = previousCardTypesAndTypeIdsMustCorrespond;
			return output;
		}

		public override void Attach (Card card)
		{
			base.Attach (card);
			if (trs.eulerAngles.z == 180)
			{
				affectedCard.trs.eulerAngles = trs.eulerAngles;
				affectedCard.type += ROTATED_TYPE_INDICATOR;
			}
		}
	}
}