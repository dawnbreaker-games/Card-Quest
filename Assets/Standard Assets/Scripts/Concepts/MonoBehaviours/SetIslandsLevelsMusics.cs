using UnityEngine;
using IslandsLevelEntry = CardQuest.IslandsLevelsData.IslandsLevelEntry;
using IslandsLevelZone = CardQuest.IslandsLevelsData.IslandsLevelZone;

namespace CardQuest
{
	[ExecuteInEditMode]
	public class SetIslandsLevelsMusics : MonoBehaviour
	{
		public IslandsLevelsData islandsLevelsData;

		void OnEnable ()
		{
			int levelIndex = 0;
			foreach (IslandsLevelZone zone in islandsLevelsData.levelZones)
			{
				for (int i = 0; i < zone.levelCount; i ++)
				{
					islandsLevelsData.islandsLevelEntries[levelIndex].musics = zone.firstLevelEntry.musics;
					levelIndex ++;
				}
			}
		}
	}
}