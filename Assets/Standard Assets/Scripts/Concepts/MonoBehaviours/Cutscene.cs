using UnityEngine;
using DialogAndStory;
using UnityEngine.Events;

namespace CardQuest
{
	public class Cutscene : MonoBehaviour
	{
		public Conversation conversation;

		public void View ()
		{
			for (int i = 0; i < CutsceneManager.instance.cutscenes.Length; i ++)
			{
				Cutscene cutscene = CutsceneManager.instance.cutscenes[i];
				cutscene.gameObject.SetActive(false);
			}
			Dialog[] dialogs = conversation.dialogs;
			UnityEvent unityEvent = dialogs[dialogs.Length - 1].onEndedEvent.unityEvent;
			unityEvent.RemoveAllListeners();
			unityEvent.AddListener(() => { GameManager.instance.UnloadScene ("Cutscenes"); });
			gameObject.SetActive(true);
		}
	}
}