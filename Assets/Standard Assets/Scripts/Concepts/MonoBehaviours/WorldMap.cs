﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Extensions;
using System;
using IslandsLevelEntry = CardQuest.IslandsLevelsData.IslandsLevelEntry;
using IslandsLevelZone = CardQuest.IslandsLevelsData.IslandsLevelZone;
using Random = UnityEngine.Random;

namespace CardQuest
{
	[ExecuteInEditMode]
	public class WorldMap : UpdateWhileEnabled
	{
		public bool PauseWhileUnfocused
		{
			get
			{
				return true;
			}
		}
		public static Zone selectedZone;
		public IslandsLevelsData islandsLevelsData;
		public Zone[] zones = new Zone[0];
		public _Text starsText;
		public AudioClip[] unlockSounds = new AudioClip[0];
		public AudioClip[] cantGoToZoneSounds = new AudioClip[0];
		bool hasPlayedUnlockSound;
		
		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				foreach (Zone zone in zones)
				{
					zone.lineRenderer.positionCount = zone.polygonCollider.points.Length;
					for (int i = 0; i < zone.polygonCollider.points.Length; i ++)
						zone.lineRenderer.SetPosition(i, zone.polygonCollider.points[i]);
				}
				return;
			}
#endif
			// SaveAndLoadManager.instance.LoadMostRecent ();
			starsText.text.text = "" + GameManager.Stars;
			for (int i = 0; i < zones.Length; i ++)
			{
				Zone zone = zones[i];
				if (GameManager.Stars >= islandsLevelsData.levelZones[i].starsRequiredToUnlockMe)
				{
					if (zone.FirstTimeUnlocking)
					{
						zone.FirstTimeUnlocking = false;
						if (!string.IsNullOrEmpty(zone.activateGoNameForeverOnFirstTimeUnlocked))
						{
							CutsceneManager.UnlockedCutsceneCount ++;
							GameManager.instance.ActivateGoForever (zone.activateGoNameForeverOnFirstTimeUnlocked);
						}
						if (!string.IsNullOrEmpty(zone.deactivateGoNameForeverOnFirstTimeUnlocked))
							GameManager.instance.DeactivateGoForever (zone.deactivateGoNameForeverOnFirstTimeUnlocked);
						// SaveAndLoadManager.instance.Save ();
						GameManager.instance.LoadScene ("Cutscenes");
						return;
					}
					zone.lockGo.SetActive(false);
				}
			}
			selectedZone = null;
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			foreach (Zone zone in zones)
			{
				if (!hasPlayedUnlockSound && !zone.FirstTimeUnlocking && zone.FirstTimeEntering)
				{
					AudioClip unlockSound = unlockSounds[Random.Range(0, unlockSounds.Length)];
					AudioManager.instance.PlaySoundEffect (null, new SoundEffect.Settings(unlockSound));
					hasPlayedUnlockSound = true;
				}
				if (zone.polygonCollider.OverlapPoint(CameraScript.instance.camera.ScreenToWorldPoint(InputManager.MousePosition)))
				{
					if (zone != selectedZone)
					{
						zone.lineRenderer.enabled = true;
						if (selectedZone != null)
							selectedZone.lineRenderer.enabled = false;
						selectedZone = zone;
					}
					if (InputManager.LeftClickInput)
					{
						if (!zone.lockGo.activeSelf)
						{
							AudioManager.instance.PlaySoundEffect ();
							LevelSelectMenu.currentZoneIndex = zone.trs.GetSiblingIndex();
							if (zone.FirstTimeEntering)
							{
								zone.FirstTimeEntering = false;
								if (!string.IsNullOrEmpty(zone.activateGoNameForeverOnFirstTimeEntered))
								{
									CutsceneManager.UnlockedCutsceneCount ++;
									GameManager.instance.ActivateGoForever (zone.activateGoNameForeverOnFirstTimeEntered);
								}
								if (!string.IsNullOrEmpty(zone.deactivateGoNameForeverOnFirstTimeEntered))
									GameManager.instance.DeactivateGoForever (zone.deactivateGoNameForeverOnFirstTimeEntered);
								// SaveAndLoadManager.instance.Save ();
								GameManager.instance.LoadScene ("Cutscenes");
								return;
							}
							GameManager.instance.LoadScene ("Level Select");
						}
						else
						{
							AudioClip cantGoToZoneSound = cantGoToZoneSounds[Random.Range(0, cantGoToZoneSounds.Length)];
							AudioManager.instance.PlaySoundEffect (null, new SoundEffect.Settings(cantGoToZoneSound));
						}
					}
					return;
				}
			}
		}

		[Serializable]
		public class Zone
		{
			public bool FirstTimeUnlocking
			{
				get
				{
					return PlayerPrefs.GetInt(trs.name + " first time unlocking", 1) == 1;
				}
				set
				{
					PlayerPrefs.SetInt(trs.name + " first time unlocking", value.GetHashCode());
				}
			}
			public bool FirstTimeEntering
			{
				get
				{
					return PlayerPrefs.GetInt(trs.name + " first time entering", 1) == 1;
				}
				set
				{
					PlayerPrefs.SetInt(trs.name + " first time entering", value.GetHashCode());
				}
			}
			public string activateGoNameForeverOnFirstTimeUnlocked;
			public string deactivateGoNameForeverOnFirstTimeUnlocked;
			public string activateGoNameForeverOnFirstTimeEntered;
			public string deactivateGoNameForeverOnFirstTimeEntered;
			public Transform trs;
			public GameObject lockGo;
			public LineRenderer lineRenderer;
			public PolygonCollider2D polygonCollider;
		} 
	}
}