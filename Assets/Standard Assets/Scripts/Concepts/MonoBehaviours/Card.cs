﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class Card : MonoBehaviour
	{
		public Transform trs;
		public SpriteRenderer spriteRenderer;
		public CardGroup[] groupsIAmPartOf = new CardGroup[0];
		public Vector2Int position;
		public string type;
		public CardSlot cardSlotUnderMe;
		public CardModifier[] cardModifiers = new CardModifier[0];
		public new Collider2D collider;

		public T GetModifier<T> () where T : CardModifier
		{
			for (int i = 0; i < cardModifiers.Length; i ++)
			{
				CardModifier cardModifier = cardModifiers[i];
				T output = cardModifier as T;
				if (output != null)
					return output;
			}
			return null;
		}
	}
}