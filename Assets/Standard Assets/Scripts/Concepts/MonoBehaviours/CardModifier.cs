using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace CardQuest
{
	public class CardModifier : MonoBehaviour
	{
		public Card affectedCard;
		public Transform trs;
		public bool onlyAffectAfterMove;
		public byte typeId;
		public bool canOnlyHave1PerCard;
		public bool cardTypesAndTypeIdsMustCorrespond;
		public bool sameTypeIdsMustBeOnEachIsland;

		public virtual void ApplyEffect ()
		{
		}

		public virtual bool CanAttach (Card card)
		{
			if (canOnlyHave1PerCard)
			{
				for (int i = 0; i < card.cardModifiers.Length; i ++)
				{
					CardModifier cardModifier = card.cardModifiers[i];
					if (cardModifier.GetType() == GetType())
						return false;
				}
			}
			if (cardTypesAndTypeIdsMustCorrespond)
			{
				IslandsLevel islandsLevel = card.trs.parent.parent.GetComponent<IslandsLevel>();
				List<CardModifier> otherInstances = new List<CardModifier>(islandsLevel.GetComponentsInChildren<CardModifier>(true));
				otherInstances.Remove(this);
				for (int i = 0; i < otherInstances.Count; i ++)
				{
					CardModifier cardModifier = otherInstances[i];
					if ((cardModifier.typeId == typeId) != (cardModifier.affectedCard.type == card.type))
						return false;
				}
			}
			if (sameTypeIdsMustBeOnEachIsland)
			{
				Island island = card.trs.parent.GetComponent<Island>();
				IslandsLevel islandsLevel = island.trs.parent.GetComponent<IslandsLevel>();
				List<Island> islandsFound = new List<Island>(new Island[] { card.trs.parent.GetComponent<Island>() });
				int sameTypeIdsFoundCount = 0;
				List<CardModifier> sameTypeIdCardModifiers = new List<CardModifier>();
				for (int i = 0; i < islandsLevel.cardModifiers.Length; i ++)
				{
					CardModifier cardModifier = islandsLevel.cardModifiers[i];
					if (cardModifier.typeId == typeId)
					{
						sameTypeIdCardModifiers.Add(cardModifier);
						sameTypeIdsFoundCount ++;
					}
				}
				int requiredIslandsFoundCount = Mathf.Min(islandsLevel.cardGroups.Length, sameTypeIdsFoundCount + 1);
				if (requiredIslandsFoundCount == 1)
					return true;
				for (int i = 0; i < sameTypeIdCardModifiers.Count; i ++)
				{
					CardModifier cardModifier = sameTypeIdCardModifiers[i];
					Island _island = cardModifier.affectedCard.trs.parent.GetComponent<Island>();
					if (!islandsFound.Contains(island))
					{
						islandsFound.Add(island);
						print(islandsFound.Count);
						if (islandsFound.Count >= requiredIslandsFoundCount)
							return true;
					}
				}
				return false;
			}
			return true;
		}

		public virtual void Attach (Card card)
		{
			trs.SetParent(card.trs);
			trs.localScale = Vector3.one;
			trs.localPosition = Vector3.zero;
			affectedCard = card;
			card.cardModifiers = card.cardModifiers.Add(this);
		}
	}
}