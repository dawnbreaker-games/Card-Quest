using System;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace CardQuest
{
	public class DRODMinigame : SingletonUpdateWhileEnabled<DRODMinigame>
	{
		Vector2 moveInput;
		Vector2 previousMoveInput;

		public override void DoUpdate ()
		{
			moveInput = InputManager.MoveInput;
			if (moveInput != Vector2.zero && previousMoveInput == Vector2.zero)
				DRODPlayer.instance.Move ((int) moveInput.x);
			previousMoveInput = moveInput;
		}

		public void OnAct ()
		{
			for (int i = 0; i < DRODEnemy.instances.Length; i ++)
			{
				DRODEnemy enemy = DRODEnemy.instances[i];
				enemy.OnAct ();
			}
		}
	}
}