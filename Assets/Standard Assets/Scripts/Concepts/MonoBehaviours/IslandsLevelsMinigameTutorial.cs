using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using IslandsLevelZone = CardQuest.IslandsLevelsData.IslandsLevelZone;
using IslandsLevelEntry = CardQuest.IslandsLevelsData.IslandsLevelEntry;

namespace CardQuest
{
	public class IslandsLevelsMinigameTutorial : IslandsLevelsMinigame
	{
		public GameObject endPanel;

		public override IEnumerator Start ()
		{
			zoneEndLevelIndex = islandsLevelsData.islandsLevelEntries.Length;
			yield return StartCoroutine(base.Start ());
		}

		public override void GoToNextLevel ()
		{
			if (currentLevelIndex == zoneEndLevelIndex - 1)
			{
				endPanel.SetActive(true);
				return;
			}
			Destroy(islandsLevels[currentLevelIndex].gameObject);
			currentLevelIndex ++;
			GoToLevel (currentLevelIndex);
		}
	}
}