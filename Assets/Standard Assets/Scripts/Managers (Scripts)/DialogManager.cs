using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DialogAndStory
{
	public class DialogManager : SingletonMonoBehaviour<DialogManager>
	{
		static Conversation currentConversation;

		public void EndCurrentDialog ()
		{
			EndDialog (currentConversation.currentDialog);
		}

		public void StartDialog (Dialog dialog)
		{
			currentConversation = dialog.conversation;
			dialog.onStartedEvent.Do ();
			dialog.IsActive = true;
		}
		
		public void EndDialog (Dialog dialog)
		{
			if (!dialog.isFinished)
				dialog.onLeftWhileTalkingEvent.Do ();
			dialog.onEndedEvent.Do ();
			dialog.IsActive = false;
		}
		
		public void StartConversation (Conversation conversation)
		{
			currentConversation = conversation;
			conversation.updateRoutine = conversation.StartCoroutine(conversation.UpdateRoutine ());
		}

		public void EndConversation (Conversation conversation)
		{
			EndDialog (conversation.currentDialog);
			conversation.StopCoroutine(conversation.updateRoutine);
		}
	}
}