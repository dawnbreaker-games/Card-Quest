using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class AudioManager : SingletonMonoBehaviour<AudioManager>, ISaveableAndLoadable
	{
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int uniqueId;
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		[SaveAndLoadValue(false)]
		public float volume;
		[SaveAndLoadValue(false)]
		public bool mute;
		public SoundEffect soundEffectPrefab;
		public SoundEffect.Settings defaultSettings;

		public override void Awake ()
		{
			base.Awake ();
			UpdateAudioListener ();
		}

		void UpdateAudioListener ()
		{
			if (mute)
				AudioListener.volume = 0;
			else
				AudioListener.volume = volume;
		}

		public void SetVolume (float volume)
		{
			if (AudioManager.instance != this)
			{
				AudioManager.instance.SetVolume (volume);
				return;
			}
			this.volume = volume;
			UpdateAudioListener ();
		}

		public void SetMute (bool mute)
		{
			if (AudioManager.instance != this)
			{
				AudioManager.instance.SetMute (mute);
				return;
			}
			this.mute = mute;
			UpdateAudioListener ();

		}

		public void ToggleMute ()
		{
			SetMute (!mute);
		}
		
		public SoundEffect PlaySoundEffect (SoundEffect soundEffectPrefab = null, SoundEffect.Settings settings = null, Vector2 position = default(Vector2), Quaternion rotation = default(Quaternion), Transform parent = null)
		{
			if (soundEffectPrefab == null)
				soundEffectPrefab = this.soundEffectPrefab;
			SoundEffect output = ObjectPool.Instance.SpawnComponent<SoundEffect>(soundEffectPrefab.prefabIndex, position, rotation, parent);
			if (settings == null)
				settings = defaultSettings;
			output.audioSource.clip = settings.clip;
			output.audioSource.volume = settings.volume;
			output.audioSource.pitch = settings.pitch;
			output.audioSource.Play();
			ObjectPool.instance.DelayDespawn (output.prefabIndex, output.gameObject, output.trs, settings.clip.length);
			return output;
		}
	}
}