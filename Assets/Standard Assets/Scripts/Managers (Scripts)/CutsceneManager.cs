using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace CardQuest
{
	public class CutsceneManager : SingletonMonoBehaviour<CutsceneManager>
	{
		public Cutscene[] cutscenes = new Cutscene[0];
		public static int UnlockedCutsceneCount
		{
			get
			{
				return PlayerPrefs.GetInt("Unlocked cutscenes", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Unlocked cutscenes", value);
			}
		}

		public void ViewCutscene (int cutsceneIndex)
		{
			if (instance != this)
			{
				instance.ViewCutscene (cutsceneIndex);
				return;
			}
			GameManager.instance.LoadSceneAdditive ("Cutscenes");
			EventSystem.current.StartCoroutine(ViewCutsceneRoutine (cutsceneIndex));
		}

		IEnumerator ViewCutsceneRoutine (int cutsceneIndex)
		{
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			Instance.cutscenes[cutsceneIndex].View ();
		}
	}
}