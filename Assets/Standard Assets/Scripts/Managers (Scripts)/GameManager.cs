using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using Object = UnityEngine.Object;

namespace CardQuest
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
			}
		}
		public int UniqueId
		{
			get
			{
				return uniqueId;
			}
			set
			{
				uniqueId = value;
			}
		}
		public static float UnscaledDeltaTime
		{
			get
			{
				if (paused || framesSinceLoadedScene <= LAG_FRAMES_AFTER_LOAD_SCENE)
					return 0;
				else
					return Time.unscaledDeltaTime;
			}
		}
		public static bool HasPlayedBefore
		{
			get
			{
				return PlayerPrefs.GetInt("Has played before", 0) == 1;
			}
			set
			{
				PlayerPrefs.SetInt("Has played before", value.GetHashCode());
			}
		}
		public static bool paused;
		// [SaveAndLoadValue(false)]
		// public static string enabledGosString = "";
		// [SaveAndLoadValue(false)]
		// public static string disabledGosString = "";
		public static string EnabledGosString
		{
			get
			{
				return PlayerPrefs.GetString("Enabled gos", "");
			}
			set
			{
				PlayerPrefs.SetString("Enabled gos", value);
			}
		}
		public static string DisabledGosString
		{
			get
			{
				return PlayerPrefs.GetString("Disabled gos", "");
			}
			set
			{
				PlayerPrefs.SetString("Disabled gos", value);
			}
		}
		public const string STRING_SEPERATOR = "|";
		public const string STRING_SEPERATOR_2 = ",";
		// public const char UNIQUE_ID_SEPERATOR = ',';
		public const int LAG_FRAMES_AFTER_LOAD_SCENE = 2;
		public static IUpdatable[] updatables = new IUpdatable[0];
		// public static IUpdatable[] pausedUpdatables = new IUpdatable[0];
		// public static Dictionary<Type, object> singletons = new Dictionary<Type, object>();
		// static float lowPassFilterFactor;
		public static int framesSinceLoadedScene;
		public static event OnGameScenesLoaded onGameScenesLoaded;
		public static bool initialized;
		static Vector3 lowPassValue;
		public static Vector2 previousMousePosition;
		public delegate void OnGameScenesLoaded();
		public static Dictionary<string, CursorEntry> cursorEntriesDict = new Dictionary<string, CursorEntry>();
		public static CursorEntry activeCursorEntry;
		public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		// [SaveAndLoadValue(false)]
		// public static int stars;
		public static int Stars
		{
			get
			{
				return PlayerPrefs.GetInt("Stars", 0);
			}
			set
			{
				PlayerPrefs.SetInt("Stars", value);
			}
		}
		public GameObject[] registeredGos = new GameObject[0];
		public Animator screenEffectAnimator;
		public CursorEntry[] cursorEntries;
		public RectTransform cursorCanvas;
		public GameModifier[] gameModifiers;
		public Timer hideCursorTimer;
		public GameScene[] gameScenes;
		public Canvas[] canvases = new Canvas[0];
		public GameObject emptyGoPrefab;
		public TemporaryActiveText notificationText;
		public IslandsLevel islandsLevelPrefab;
		public Island islandPrefab;
		public Card[] islandsLevelCardPrefabs = new Card[0];
		public CardSlot cardSlotPrefab;
		public static Dictionary<string, Card> cardTypesDict = new Dictionary<string, Card>();
		public float timeScale;
		public Team[] teams;
		public int uniqueId;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				framesSinceLoadedScene = 0;
				return;
			}
			// else
			// {
			// 	for (int i = 0; i < gameScenes.Length; i ++)
			// 	{
			// 		if (!gameScenes[i].use)
			// 		{
			// 			gameScenes = gameScenes.RemoveAt(i);
			// 			i --;
			// 		}
			// 	}
			// }
#endif
			base.Awake ();
			if (instance != this)
				return;
			cardTypesDict.Clear();
			for (int i = 0; i < islandsLevelCardPrefabs.Length; i ++)
			{
				Card islandsLevelCard = islandsLevelCardPrefabs[i];
				cardTypesDict.Add(islandsLevelCard.type, islandsLevelCard);
			}
			// singletons.Remove(GetType());
			// singletons.Add(GetType(), this);
			// InitCursor ();
			AccountManager.lastUsedAccountIndex = 0;
			if (SceneManager.GetActiveScene().name == "Init")
				LoadGameScenes ();
			StartCoroutine(OnGameSceneLoadedRoutine ());
		}

		void Init ()
		{
			initialized = true;
		}
		
		public IEnumerator OnGameSceneLoadedRoutine ()
		{
			gameModifierDict.Clear();
			foreach (GameModifier gameModifier in gameModifiers)
				gameModifierDict.Add(gameModifier.name, gameModifier);
			hideCursorTimer.onFinished += HideCursor;
			if (screenEffectAnimator != null)
				screenEffectAnimator.Play("None");
			// GetSingleton<PauseMenu>().Hide ();
			if (AccountManager.lastUsedAccountIndex != -1)
			{
				// GetSingleton<AccountSelectMenu>().gameObject.SetActive(false);
				PauseGame (false);
			}
			// canvases = FindObjectsOfType<Canvas>();
			// foreach (Canvas canvas in canvases)
			// 	canvas.worldCamera = GetSingleton<GameCamera>().camera;
			if (onGameScenesLoaded != null)
			{
				onGameScenesLoaded ();
				onGameScenesLoaded = null;
			}
			yield return StartCoroutine(LoadRoutine ());
			yield break;
		}

		void Update ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
				if (!initialized)
					return;
			// try
			// {
				InputSystem.Update ();
				foreach (IUpdatable updatable in updatables)
					updatable.DoUpdate ();
				// Physics2D.Simulate(Time.deltaTime);
				ObjectPool.instance.DoUpdate ();
				CameraScript.instance.DoUpdate ();
				// GetSingleton<GameCamera>().DoUpdate ();
				// acceleration = InputManager.Acceleration;
				// lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
				// if ((acceleration - lowPassValue).sqrMagnitude >= shakeDetectionThreshold || Input.GetKeyDown(KeyCode.Escape))
					// GetSingleton<PauseMenu>().Open ();
				framesSinceLoadedScene ++;
				previousMousePosition = InputManager.MousePosition;
			// }
			// catch (Exception e)
			// {
			// 	Debug.Log(e.Message + "\n" + e.StackTrace);
			// }
		}

		public virtual void InitCursor ()
		{
			cursorEntriesDict.Clear();
			foreach (CursorEntry cursorEntry in cursorEntries)
			{
				cursorEntriesDict.Add(cursorEntry.name, cursorEntry);
				cursorEntry.rectTrs.gameObject.SetActive(false);
			}
			// Cursor.visible = false;
			activeCursorEntry = null;
			// cursorEntriesDict["Default"].SetAsActive ();
		}

		public virtual IEnumerator LoadRoutine ()
		{
			yield return new WaitForEndOfFrame();
			SaveAndLoadManager.instance.Setup ();
			if (!HasPlayedBefore)
			{
				SaveAndLoadManager.instance.DeleteAll ();
				HasPlayedBefore = true;
				SaveAndLoadManager.instance.OnLoaded ();
			}
			else
				SaveAndLoadManager.instance.LoadMostRecent ();
			// GetSingleton<AdsManager>().ShowAd ();
			SetGosActive ();
			Init ();
			yield break;
		}

		public virtual void HideCursor (params object[] args)
		{
			activeCursorEntry.rectTrs.gameObject.SetActive(false);
		}

		public virtual void LoadScene (string name)
		{
			if (instance != this)
			{
				instance.LoadScene (name);
				return;
			}
			framesSinceLoadedScene = 0;
			SceneManager.LoadScene(name);
		}

		public virtual void LoadSceneAdditive (string name)
		{
			if (instance != this)
			{
				instance.LoadSceneAdditive (name);
				return;
			}
			SceneManager.LoadScene(name, LoadSceneMode.Additive);
		}

		public virtual void LoadScene (int index)
		{
			LoadScene (SceneManager.GetSceneByBuildIndex(index).name);
		}

		public virtual void UnloadScene (string name)
		{
			AsyncOperation unloadGameScene = SceneManager.UnloadSceneAsync(name);
			unloadGameScene.completed += OnGameSceneUnloaded;
		}

		public virtual void OnGameSceneUnloaded (AsyncOperation unloadGameScene)
		{
			unloadGameScene.completed -= OnGameSceneUnloaded;
		}

		public virtual void ReloadActiveScene ()
		{
			LoadScene (SceneManager.GetActiveScene().name);
		}

		public virtual void LoadGameScenes ()
		{
			if (instance != this)
			{
				instance.LoadGameScenes ();
				return;
			}
			initialized = false;
			StopAllCoroutines ();
			if (gameScenes.Length == 0 || SceneManager.GetSceneByName(gameScenes[0].name).isLoaded)
			{
				// UnloadScene ("Game");
				// LoadSceneAdditive ("Game");
				return;
			}
			LoadScene (gameScenes[0].name);
			GameScene gameScene;
			for (int i = 1; i < gameScenes.Length; i ++)
			{
				gameScene = gameScenes[i];
				if (gameScene.use)
					LoadSceneAdditive (gameScene.name);
			}
		}

		public virtual void PauseGame (bool pause)
		{
			paused = pause;
			Time.timeScale = timeScale * (1 - paused.GetHashCode());
			// AudioListener.pause = paused;
		}

		public virtual void Quit ()
		{
			Application.Quit();
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#endif
		}

		public virtual void OnApplicationQuit ()
		{
			if (AccountManager.lastUsedAccountIndex != -1)
				AccountManager.CurrentlyPlaying.PlayTime += Time.time;
			SaveAndLoadManager.instance.Save ();
		}

		public virtual void SetGosActive ()
		{
			if (instance != this)
			{
				instance.SetGosActive ();
				return;
			}
			string[] stringSeperators = { STRING_SEPERATOR };
			if (EnabledGosString == null)
				EnabledGosString = "";
			string[] enabledGos = EnabledGosString.Split(stringSeperators, StringSplitOptions.None);
			foreach (string goName in enabledGos)
			{
				for (int i = 0; i < registeredGos.Length; i ++)
				{
					GameObject registeredGo = registeredGos[i];
					if (goName == registeredGo.name)
					{
						registeredGo.SetActive(true);
						break;
					}
				}
			}
			if (DisabledGosString == null)
				DisabledGosString = "";
			string[] disabledGos = DisabledGosString.Split(stringSeperators, StringSplitOptions.None);
			foreach (string goName in disabledGos)
			{
				GameObject go = GameObject.Find(goName);
				if (go != null)
					go.SetActive(false);
			}
		}
		
		public virtual void ActivateGoForever (GameObject go)
		{
			go.SetActive(true);
			ActivateGoForever (go.name);
		}
		
		public virtual void DeactivateGoForever (GameObject go)
		{
			go.SetActive(false);
			DeactivateGoForever (go.name);
		}
		
		public virtual void ActivateGoForever (string goName)
		{
			DisabledGosString = DisabledGosString.Replace(STRING_SEPERATOR + goName, "");
			if (!EnabledGosString.Contains(goName))
				EnabledGosString += STRING_SEPERATOR + goName;
		}
		
		public virtual void DeactivateGoForever (string goName)
		{
			EnabledGosString = EnabledGosString.Replace(STRING_SEPERATOR + goName, "");
			if (!DisabledGosString.Contains(goName))
				DisabledGosString += STRING_SEPERATOR + goName;
		}

		public virtual void SetGameObjectActive (string name)
		{
			GameObject.Find(name).SetActive(true);
		}

		public virtual void SetGameObjectInactive (string name)
		{
			GameObject.Find(name).SetActive(false);
		}

		public virtual void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (instance != this)
				return;
			StopAllCoroutines();
			for (int i = 0; i < Timer.runningInstances.Length; i ++)
			{
				Timer timer = Timer.runningInstances[i];
				timer.Stop ();
				i --;
			}
			hideCursorTimer.onFinished -= HideCursor;
			// SceneManager.sceneLoaded -= OnSceneLoaded;
		}

		public virtual void _Log (object o)
		{
			print(o);
		}

		public static void Log (object o)
		{
			print(o);
		}

		public static Object Clone (Object obj)
		{
			return Instantiate(obj);
		}

		public static Object Clone (Object obj, Transform parent)
		{
			return Instantiate(obj, parent);
		}

		public static Object Clone (Object obj, Vector3 position, Quaternion rotation)
		{
			return Instantiate(obj, position, rotation);
		}

		public static void _Destroy (Object obj)
		{
			Destroy(obj);
		}

		public static void _DestroyImmediate (Object obj)
		{
			DestroyImmediate(obj);
		}

		public virtual void ToggleGo (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public virtual void PressButton (Button button)
		{
			button.onClick.Invoke();
		}

		// public static T GetSingleton<T> ()
		// {
		// 	if (!singletons.ContainsKey(typeof(T)))
		// 		return GetSingleton<T>(FindObjectsOfType<Object>());
		// 	else
		// 	{
		// 		if (singletons[typeof(T)] == null || singletons[typeof(T)].Equals(default(T)))
		// 		{
		// 			T singleton = GetSingleton<T>(FindObjectsOfType<Object>());
		// 			singletons[typeof(T)] = singleton;
		// 			return singleton;
		// 		}
		// 		else
		// 			return (T) singletons[typeof(T)];
		// 	}
		// }

		// public static T GetSingleton<T> (Object[] objects)
		// {
		// 	if (typeof(T).IsSubclassOf(typeof(Object)))
		// 	{
		// 		foreach (Object obj in objects)
		// 		{
		// 			if (obj is T)
		// 			{
		// 				singletons.Remove(typeof(T));
		// 				singletons.Add(typeof(T), obj);
		// 				break;
		// 			}
		// 		}
		// 	}
		// 	if (singletons.ContainsKey(typeof(T)))
		// 		return (T) singletons[typeof(T)];
		// 	else
		// 		return default(T);
		// }

		// public static T GetSingletonIncludeAssets<T> ()
		// {
		// 	if (!singletons.ContainsKey(typeof(T)))
		// 		return GetSingletonIncludeAssets<T>(FindObjectsOfTypeIncludingAssets(typeof(T)));
		// 	else
		// 	{
		// 		if (singletons[typeof(T)] == null || singletons[typeof(T)].Equals(default(T)))
		// 		{
		// 			T singleton = GetSingletonIncludeAssets<T>(FindObjectsOfTypeIncludingAssets(typeof(T)));
		// 			singletons[typeof(T)] = singleton;
		// 			return singleton;
		// 		}
		// 		else
		// 			return (T) singletons[typeof(T)];
		// 	}
		// }

		// public static T GetSingletonIncludeAssets<T> (object[] objects)
		// {
		// 	if (typeof(T).IsSubclassOf(typeof(object)))
		// 	{
		// 		foreach (Object obj in objects)
		// 		{
		// 			if (obj is T)
		// 			{
		// 				singletons.Remove(typeof(T));
		// 				singletons.Add(typeof(T), obj);
		// 				break;
		// 			}
		// 		}
		// 	}
		// 	if (singletons.ContainsKey(typeof(T)))
		// 		return (T) singletons[typeof(T)];
		// 	else
		// 		return default(T);
		// }

		public static bool ModifierIsActiveAndExists (string name)
		{
			GameModifier gameModifier;
			if (gameModifierDict.TryGetValue(name, out gameModifier))
				return gameModifier.isActive;
			else
				return false;
		}

		public static bool ModifierIsActive (string name)
		{
			return gameModifierDict[name].isActive;
		}

		public static bool ModifierExists (string name)
		{
			return gameModifierDict.ContainsKey(name);
		}

		[Serializable]
		public class CursorEntry
		{
			public string name;
			public RectTransform rectTrs;

			public virtual void SetAsActive ()
			{
				if (activeCursorEntry != null)
					activeCursorEntry.rectTrs.gameObject.SetActive(false);
				rectTrs.gameObject.SetActive(true);
				activeCursorEntry = this;
			}
		}

		[Serializable]
		public class GameModifier
		{
			public string name;
			public bool isActive;
		}

		[Serializable]
		public class GameScene
		{
			public string name;
			public bool use = true;
		}
	}
}