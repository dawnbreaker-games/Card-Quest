#if UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;
using Extensions;

namespace CardQuest
{
	[ExecuteInEditMode]
	public class SetCameraRectToAllCardsBounds : MonoBehaviour
	{
		public bool update;

		void Update ()
		{
			if (!update)
				return;
			update = false;
			Card[] cards = FindObjectsOfType<Card>();
			List<Rect> cardRects = new List<Rect>();
			foreach (Card card in cards)
				cardRects.Add(card.spriteRenderer.bounds.ToRect());
			CameraScript.instance.viewRect = RectExtensions.Combine(cardRects.ToArray());
			CameraScript.instance.trs.position = CameraScript.instance.viewRect.center.SetZ(CameraScript.instance.trs.position.z);
			CameraScript.instance.viewSize = CameraScript.instance.viewRect.size;
			CameraScript.instance.HandleViewSize ();
		}
	}
}
#endif