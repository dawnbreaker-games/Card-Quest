using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class Scorp : DRODEnemy
	{
		public override void OnAct ()
		{
			Move (MathfExtensions.Sign(DRODPlayer.instance.trs.position.x - trs.position.x));
		}
	}
}