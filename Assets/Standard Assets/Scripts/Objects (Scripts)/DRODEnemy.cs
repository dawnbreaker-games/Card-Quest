using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class DRODEnemy : SingletonMonoBehaviour<DRODEnemy>
	{
		public Transform trs;
		public static DRODEnemy[] instances = new DRODEnemy[0];

		void OnEnable ()
		{
			instances = instances.Add(this);
		}

		public virtual void OnAct ()
		{
			
		}

		public void Move (int direction)
		{
			if (direction != 0)
			{
				trs.position += Vector3.right * direction;
			}
		}

		void OnDisable ()
		{
			instances = instances.Remove(this);
		}
	}
}