using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CardQuest
{
	public class DRODPlayer : SingletonMonoBehaviour<DRODPlayer>
	{
		public Transform trs;

		public void Move (int direction)
		{
			if (direction != 0)
			{
				trs.position += Vector3.right * direction;
			}
			DRODMinigame.instance.OnAct ();
		}
	}
}